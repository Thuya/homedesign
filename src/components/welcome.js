import React, { Component } from 'react'
import { Carousel } from 'react-bootstrap'
import { Wave } from 'react-animated-text';
// import {Animated} from "react-animated-css"

class Welcome extends Component {
    constructor(props, context) {
        super(props, context);
    
        this.handleSelect = this.handleSelect.bind(this);
    
        this.state = {
            index: 0,
            direction: null,
            fade: false,
            indicators: false
        };
    }
    
    handleSelect(selectedIndex, e) {
        this.setState({
            index: selectedIndex,
            direction: e.direction,
            fade: true
        });
     }
    
    render() {
        const { index, direction, fade, indicators } = this.state;
    
        return (
            <Carousel
                activeIndex={index}
                direction={direction}
                onSelect={this.handleSelect}
                fade={fade}
                indicators={indicators}
                className="welcome"
            >
                <Carousel.Item className="slide-img">
                    <div className="welcome-img-1 d-block w-100">
                        <div className="overlay w-100">
                            <Carousel.Caption>
                                <Wave className="text-1"
                                    text="Architecture is a Visual Art."
                                    effect="verticalFadeIn"
                                    effectChange={2.5}
                                    effectDirection='down'
                                    iterations={1}
                                    paused={this.state.paused}
                                >
                                </Wave>
                                <Wave className="text-2"
                                    text="The building speaks for themselves."
                                    effect="verticalFadeIn"
                                    effectChange={2.5}
                                    effectDirection='left'
                                    iterations={1}
                                    paused={this.state.paused}
                                >
                                </Wave>
                            </Carousel.Caption>
                        </div>
                    </div>
                </Carousel.Item>

                <Carousel.Item className="slide-img">
                    <div className="welcome-img-2 d-block w-100">
                        <div className="overlay w-100">
                            <Carousel.Caption>
                            <Wave className="font-weight-bolder text-left"
                                    text="Architecture is a Visual Art."
                                    effect="verticalFadeIn"
                                    effectChange={2.5}
                                    effectDirection='down'
                                    iterations={1}
                                    paused={this.state.paused}
                                >
                                </Wave>
                                <Wave className="font-weight-bolder text-left"
                                    text="The building speaks for themselves."
                                    effect="verticalFadeIn"
                                    effectChange={2.5}
                                    effectDirection='left'
                                    iterations={1}
                                    paused={this.state.paused}
                                >
                                </Wave>
                            </Carousel.Caption>
                        </div>
                    </div>
                </Carousel.Item>

                <Carousel.Item className="slide-img">
                    <div className="welcome-img-3 d-block w-100">
                        <div className="overlay w-100">
                            <Carousel.Caption>
                            <Wave className="font-weight-bolder text-left"
                                    text="Architecture is a Visual Art."
                                    effect="verticalFadeIn"
                                    effectChange={2.5}
                                    effectDirection='down'
                                    iterations={1}
                                    paused={this.state.paused}
                                >
                                </Wave>
                                <Wave className="font-weight-bolder text-left"
                                    text="The building speaks for themselves."
                                    effect="verticalFadeIn"
                                    effectChange={2.5}
                                    effectDirection='left'
                                    iterations={1}
                                    paused={this.state.paused}
                                >
                                </Wave>
                            </Carousel.Caption>
                        </div>
                    </div>
                </Carousel.Item>
            </Carousel>
        );
      }
}

export default Welcome