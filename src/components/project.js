import React, { Component } from 'react'
import Slider from "react-slick";
import { Container, Card, Button } from 'react-bootstrap';

// images
import project1 from "../images/IMG-0acff9164e361d73a1815eea1c76830c-V.jpg"
import project2 from "../images/IMG-1e2de3b3be80d70d6d81798a5081ac8e-V.jpg"
import project3 from "../images/IMG-2eb1f3e7db7b186b019307dd34f5b17e-V.jpg"
import project4 from "../images/IMG-3ba9a078a2a70072df68ba3db28db5e8-V.jpg"
import project5 from "../images/IMG-3e362b0df3cfc8be9bc012800180d8f8-V.jpg"
import project6 from "../images/IMG-4cbb637738649aae4487468456687f48-V.jpg"
import project7 from "../images/IMG-5df023379cc1a91ce7f9dc72ee956422-V.jpg"
import project8 from "../images/IMG-7a7bd96fb61f7eab5a93e4a7e4f774d1-V.jpg"
import project9 from "../images/IMG-7e23b1f890ae5f022d3726ea5097f653-V.jpg"
import project10 from "../images/IMG-0382ef93bebf365946843a34f1bee8a1-V.jpg"
import project11 from "../images/IMG-592e4669924e18dea7472c9724949e46-V.jpg"
import project12 from "../images/IMG-37641bb76950a6b2fa881ee6295224e2-V.jpg"
import project13 from "../images/IMG-d5c47987ff602b783d99271a063fa482-V.jpg"
import project14 from "../images/IMG-d20aa7034397bf7253a5a3707f551c4d-V.jpg"
import project15 from "../images/IMG-ff39faf9d8c3e7d33dae45d3f89579f3-V.jpg"


class Project extends Component {
    render() {
        var settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1,
            initialSlide: 0,
            arrows: false,

            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  initialSlide: 0
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]
          };
        return(
            <div className="projects position-relative mb-5 d-block">
                <div className="overlay">
                    <Container>
                        <div className="section-header mb-5 mt-3 position-relative pb-2">
                            <h1 className="font-weight-bolder">PROJECTS</h1>
                            <h2 className="position-absolute">Our Projects</h2>
                        </div>
                    </Container>
                    <Slider {...settings} className="position-absolute w-100 pb-5">
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden"> 
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project1} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project2} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project3} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project4} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project5} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project6} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project7} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project8} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project9} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project10} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project11} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project12} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project13} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project14} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card className="border-0 ml-2 mr-2 rounded-0">
                                    <div className="overflow-hidden">
                                        <Card.Img variant="top" height="288" className="rounded-0" src={project15} />
                                    </div>
                                    <Card.Body>
                                        <Card.Title className="text-center">Card Title</Card.Title>
                                        <Card.Text className="text-center text-dark">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                    </Slider>
                </div>
            </div>
        )
    }
}

export default Project