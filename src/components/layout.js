import React from "react"
import Helmet from "react-helmet"

import Header from "./header"
import Welcome from "./welcome"
import Service from "./service"
import Project from "./project"
import Testimonial from "./testimonial"
import Team from "./team"
import Footer from "./footer"

import "bootstrap/dist/css/bootstrap.css"
import "../scss/icon/fontawesome-pro/css/all.min.css"

import "../scss/app.scss"


const Layout = () => (
  <>
    <Helmet>
      <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css"
        integrity="sha384-OHBBOqpYHNsIqQy8hL1U+8OXf9hH6QRxi0+EODezv82DfnZoV7qoHAZDwMwEJvSw"
        crossorigin="anonymous" />
    </Helmet>
    <Header />

    <Welcome />

    <Service />

    <Project />

    <Testimonial />

    <Team />

    <Footer />
  </>
)

export default Layout
